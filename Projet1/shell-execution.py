import subprocess

# Exécute un script shell sans paramètres (exemple : script.sh)
subprocess.run(["../Files/script.sh"])

# Exécute un script shell avec des paramètres (exemple : parm.sh avec trois paramètres)
subprocess.run(["../Files/parm.sh", "param1", "param2", "param3"])
