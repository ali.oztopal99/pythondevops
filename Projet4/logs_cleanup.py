import os
import time

def cleanup_logs(logs_directory, days_threshold):
    current_time = time.time()
    threshold_time = current_time - (days_threshold * 24 * 60 * 60)

    print(f"Nettoyage des fichiers de logs plus vieux que {days_threshold} jours dans {logs_directory}...")

    for root, dirs, files in os.walk(logs_directory):
        for file in files:
            file_path = os.path.join(root, file)
            file_modified_time = os.path.getmtime(file_path)

            if file_modified_time < threshold_time:
                os.remove(file_path)
                print(f"Fichier supprimé : {file_path}")

    print("Nettoyage des fichiers de logs terminé.")

if __name__ == "__main__":
    logs_directory = "/var/log" 
    days_threshold = 10

    cleanup_logs(logs_directory, days_threshold)
