import psutil

def check_cpu_usage():
    cpu_usage = psutil.cpu_percent(interval=1)
    print(f"Utilisation du CPU : {cpu_usage}%")

def check_memory_usage():
    memory = psutil.virtual_memory()
    print(f"Utilisation de la mémoire : {memory.percent}%")

def check_disk_usage():
    disk = psutil.disk_usage('/')
    print(f"Espace disque utilisé : {disk.percent}%")

if __name__ == "__main__":
    print("Vérification de l'état du serveur :")
    check_cpu_usage()
    check_memory_usage()
    check_disk_usage()
