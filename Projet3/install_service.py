import subprocess

def install_apache():
    print("Installation d'Apache...")
    subprocess.run(["sudo", "apt-get", "install", "apache2"])
    print("Apache installé avec succès!")

def install_ssh():
    print("Installation d'OpenSSH Server...")
    subprocess.run(["sudo", "apt-get", "install", "openssh-server"])
    print("OpenSSH Server installé avec succès!")

if __name__ == "__main__":
    install_apache()
    install_ssh()
