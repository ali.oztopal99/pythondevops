import subprocess

def configure_apache():
    apache_config_path = "/etc/apache2/apache2.conf"

    with open(apache_config_path, 'a') as apache_conf:
        apache_conf.write("\n# Configuration personnalisée\n")

    print("Configuration appliquée.")

def restart_apache():
    print("Redémarrage d'Apache...")
    subprocess.run(["sudo", "service", "apache2", "restart"], check=True)
    print("Apache redémarré.")

if __name__ == "__main__":
    configure_apache()
    restart_apache()
