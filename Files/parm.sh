#!/bin/bash

# Affiche les paramètres passés lors de l'exécution
if [ "$#" -eq 0 ]; then
    echo "Aucun paramètre n'a été passé."
else
    echo "Nombre total de paramètres passés : $#"
    echo "Liste des paramètres passés :"

    # Utilise une boucle for pour parcourir tous les paramètres
    for param in "$@"; do
        echo "\$param = $param"
    done
    echo "Les paramètres passés sont : \$1 = $1, \$2 = $2, \$3 = $3"
fi
