import os
import json

# chemin du fichier JSON
json_file_path = "info.json"

# chemin absolu 
abs_json_file_path = os.path.abspath(json_file_path)

# Affiche le chemin du fichier JSON
print("Chemin du fichier JSON:", json_file_path)

# Affiche le chemin absolu du fichier JSON
print("Chemin absolu du fichier JSON:", abs_json_file_path)

# Lit le fichier JSON
with open(json_file_path, 'r') as file:
    data = json.load(file)

# affiche la valeur de 'name'
if 'name' in data:
    print("Valeur associée à la clé 'name':", data['name'])

# parcourt toutes les clés et valeurs du dictionnaire JSON
print("Toutes les paires clé-valeur dans le fichier JSON:")
for key, value in data.items():
    print(f"{key}: {value}")
